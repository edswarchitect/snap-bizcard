# SNAP-BIZCARD OVERVIEW
Command line tool, snap-bizcard, that is responsible for parsing the results of the OCR component to extract the name, phone number, and email address from a business card.

## Repository Location

The component can be cloned from BitBucket, using Git, using the Git command:
git clone https://edswarchitect@bitbucket.org/edswarchitect/snap-bizcard.git


---

## Building the Application

After the repository is cloned, to build the application, use:
  ./gradlew clean assembleDist

to build the distribution.

  ./gradlew assembleDist

## Verify application by running tests

This is an optional step. To verify the installation, by running the unit  tests, running
  ./gradlew tests

## Installation

1. Copy the installation package to the installation directory <install_dir>
  cp build/distributions/snap-bizcard-1.0.0.tar <install_dir>
2. Untar the installation package
  tar xvf snap-bizcard-1.0.0.tar

## Run the application
1. Change directory to <install_dir>/snap-ocr/bin
2. To run the application, ./snap-bizcard --help to print the help
./snap-bizcard --file <ocr file> to run the application for one files
./snap-bizcard --directory <path to directory containing OCR files> to run the application for all files in a directory
