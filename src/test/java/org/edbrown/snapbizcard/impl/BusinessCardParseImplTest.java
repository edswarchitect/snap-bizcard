package org.edbrown.snapbizcard.impl;

import org.edbrown.snapbizcard.ContractInfo;
import org.edbrown.snapbizcard.ExtractException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BusinessCardParseImplTest {
    BusinessCardParseImpl bc;

    @Before
    public void setUp() throws Exception {
        bc = new BusinessCardParseImpl();
    }

    @Test
    public void getContractInfo() {
    }

    @Test
    public void identifyNames() {
    }

    @Test
    public void identifyEmailAddress() {
        String[] emailAddresses = {"edwin@yahoo.org", "ed.brown@mycomp.info", "ed_brown@", "@edswarchitect", "ed123.425-yoda@goober.com" };
        String[] matches = {"edwin@yahoo.org", "ed.brown@mycomp.info", null, null, "ed123.425-yoda@goober.com"};
        String res;

        for (int i = 0; i < emailAddresses.length; i++) {

            res = bc.identifyEmailAddress(emailAddresses[i]);

            Assert.assertEquals("Email addresses don't match", res, matches[i]);
        }
    }

    @Test
    public void card0Test() {
        String doc = "ASYMMETRIK LTD\nMike Smith\nSenior Software Engineer\n(410)555-1234\nmsmith@asymmetrik.com";

        try {
            ContractInfo ci = bc.getContractInfo(doc);

            Assert.assertEquals("Names don't match", ci.getName(), "Mike Smith");
            Assert.assertEquals("Phone don't match", ci.getPhoneNumber(), "4105551234");
            Assert.assertEquals("Names don't match", ci.getEmailAddress(), "msmith@asymmetrik.com");

        } catch (ExtractException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void card1Test() {
        String doc = "Lisa Huang\nFoobar Inc.\n1234 Sentry Rd\nColumbia, MD 12345\n410-555-1234\nFax: 410-555-4321\nlisa.haung@foobartech.com";

        try {
            ContractInfo ci = bc.getContractInfo(doc);

            Assert.assertEquals("Names don't match", ci.getName(), "Lisa Huang");
            Assert.assertEquals("Phone don't match", ci.getPhoneNumber(), "4105551234");
            Assert.assertEquals("Names don't match", ci.getEmailAddress(), "lisa.haung@foobartech.com");

        } catch (ExtractException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void card1_1Test() {
        String doc = "Lisa Huang\nFoobar Inc.\n1234 Sentry Rd\nColumbia, MD 12345\nPhone: 410-555-1234\nFax: 410-555-4321\nlisa.haung@foobartech.com";

        try {
            ContractInfo ci = bc.getContractInfo(doc);

            Assert.assertEquals("Names don't match", ci.getName(), "Lisa Huang");
            Assert.assertEquals("Phone don't match", ci.getPhoneNumber(), "4105551234");
            Assert.assertEquals("Names don't match", ci.getEmailAddress(), "lisa.haung@foobartech.com");

        } catch (ExtractException e) {
            Assert.fail(e.getMessage());
        }
    }
    @Test
    public void card1_2Test() {
        String doc = "Lisa Huang\nFoobar Inc.\n1234 Sentry Rd\nColumbia, MD 12345\nTelephone: 410-555-1234\nFax: 410-555-4321\nlisa.haung@foobartech.com";

        try {
            ContractInfo ci = bc.getContractInfo(doc);

            Assert.assertEquals("Names don't match", ci.getName(), "Lisa Huang");
            Assert.assertEquals("Phone don't match", ci.getPhoneNumber(), "4105551234");
            Assert.assertEquals("Names don't match", ci.getEmailAddress(), "lisa.haung@foobartech.com");

        } catch (ExtractException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void card2Test() {
        String doc = "Arthur Wilson\nSoftware Engineer\nDecision & Security Technologies\nABC Technologies\n1234 Sentry Rd\nSuite 229\nColumbia, MD 12345\nTel: +1 (703) 555-1259\nFax: 410-555-4321\nawilson@abctech.com\n";

        try {
            ContractInfo ci = bc.getContractInfo(doc);

            Assert.assertEquals("Names don't match", ci.getName(), "Arthur Wilson");
            Assert.assertEquals("Phone don't match", ci.getPhoneNumber(), "17035551259");
            Assert.assertEquals("Names don't match", ci.getEmailAddress(), "awilson@abctech.com");

        } catch (ExtractException e) {
            Assert.fail(e.getMessage());
        }
    }


}