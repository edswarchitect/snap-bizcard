package org.edbrown.snapbizcard;

/**
 * To be thrown when there is a problem accessing the document
 */
public class DocumentException extends Exception {
}
