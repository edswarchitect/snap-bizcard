package org.edbrown.snapbizcard;

public interface BusinessCardParser {
    /*
    BusinessCardParser
    ContactInfo getContactInfo(String document)
     */
    public ContractInfo getContractInfo(String document) throws ExtractException;
}
