package org.edbrown.snapbizcard;

import org.apache.commons.cli.*;
import org.edbrown.snapbizcard.impl.BusinessCardParseImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * The main for getting the email address, name, and phone number from the OCR'ed business card
 * @author Ed Brown
 */
public class ExtractBizInfo {

    /**
     * Process the file
     * @param file The file to process
     */
    private static void processFile(String file) {

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            FileChannel channel = fis.getChannel();
            MappedByteBuffer mbb = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());

            Charset utf8 = Charset.forName("UTF8");

            CharBuffer cb = utf8.decode(mbb);

            channel.close();

            String doc = cb.toString();

            BusinessCardParser parser = new BusinessCardParseImpl();

            ContractInfo info = parser.getContractInfo(doc);

            System.out.println(doc);

            System.out.println("==>");

            System.out.format("Name: %s\nPhone: %s\nEmail: %s\n",
                    info.getName(), info.getPhoneNumber(), info.getEmailAddress());
        } catch (FileNotFoundException fnfe) {
            System.err.format("File: '%s' not found\n", file);
        } catch (ExtractException ocre) {
            System.err.format("File: '%s' unable to be processed. %s\n", file, ocre);
        } catch (IOException ioe) {
            System.err.format("File: '%s' I/O processing error. %s\n", file, ioe);
        }

    }

    /**
     * Main.
     *
     * @param args The "-d | --directory" option takes a path to a directory of documents to process. <p> The
     *             "-f | --file" option takes a path to a file
     *
     */
    public static void main(String... args) {
        Options options = new Options();
        options.addOption("f", "file", true, "The file path of the scanned business card document");
        options.addOption("d", "directory", true, "The directory that contains files to process");
        options.addOption("h", "help", false, "Print the help statement");

        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser commandLineParser = new DefaultParser();

        try {
                CommandLine cmd = commandLineParser.parse(options, args);

                if (cmd.hasOption("h")) {
                    formatter.printHelp("ExtractBizInfo", options);

                    System.exit(1);
                }

                else if (cmd.hasOption("f")) {
                    processFile(cmd.getOptionValue("f"));
                }
                else if (cmd.hasOption("d")) {
                    String dir = cmd.getOptionValue("d");

                    File directory = new File(dir);

                    if (directory.isDirectory() && directory.canRead()) {


                        File[] allFiles = directory.listFiles();

                        Arrays.stream(allFiles).forEach(file -> {
                            System.out.format("Processing file %s...\n", file.getAbsolutePath());
                            processFile(file.getAbsolutePath());
                            System.out.println("---------\n");
                        });

                    }
                    else {
                        System.err.format("The directory argument '%s' is not a directory or is not readable.\n", dir);
                        System.exit(1);
                    }

                }
        } catch (ParseException pe) {
            formatter.printHelp("ExtractBizInfo", options);
        }


    }
}
