package org.edbrown.snapbizcard.impl;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.edbrown.snapbizcard.BusinessCardParser;
import org.edbrown.snapbizcard.ContractInfo;
import org.edbrown.snapbizcard.ExtractException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implements the business card parser, using NLP
 */
public class BusinessCardParseImpl implements BusinessCardParser {
    /**
     * the sentence NLP model
     */
    private SentenceModel sentenceModel;
    /**
     * the sentence detector
     */
    private SentenceDetector sentenceDetector;
    /**
     * The entity tokenizer
     */
    private TokenizerME tokenizer;
    /**
     * the token model
     */
    private TokenizerModel tokenModel;
    /**
     * the name token model
     */
    private TokenNameFinderModel tokenNameModel;
    /**
     * the name finder
     */
    private NameFinderME nameFinder;
    /**
     * the email regex
     */
    final private static Pattern EMAIL_REGEX = Pattern.compile("^([\\w-_\\d.]+@([\\w-]+\\.)+[\\w-]{2,4}$)");
    /** The phone number REGEX */
    final private static Pattern PHONE_REGEX = Pattern.compile("^((\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*)$");
    final private static Pattern PRE_PHONE = Pattern.compile("^((Telephone|Phone|Tel):)?(.*)");

    /**
     * Constructor
     * @throws ExtractException thrown if there is an error accessing the NLP models
     */
    public BusinessCardParseImpl() throws ExtractException {
        try {
            loadModels();


        } catch (IOException ioe) {
            throw new ExtractException("Unable to load one of the NLP models.", ioe);
        }
    }

    /**
     * Get the name, phone number, and email address from the card document.
     * @param document The text to find the desired information
     * @return Returns the information found in the text. It may be incomplete if some fields are found, while others
     * aren't found. Returns null if none of the fields are found
     * @throws ExtractException Thrown if there is an error loading the NLP information
     */
    @Override
    public ContractInfo getContractInfo(String document) throws ExtractException {
        ContractInfo ci = null;
        try {
            String[] sentences = detectSentences(document);

            String name = null;
            String phone = null;
            String email = null;


            for (String sentence : sentences) {

                if (email == null) {
                    email = identifyEmailAddress(sentence);
                }

                if (phone == null) {
                    phone = identifyPhone(sentence);
                }

                String[] tokens = identifyTokens(sentence);

                if (name == null) {

                    String[] names = identifyNames(tokens);

                    if (names != null && names.length > 0) {
                        name = names[0];
                    }
                }

            } // for(String sentence : sentences) {

            if (name == null && phone == null && email == null) {
                throw new ExtractException("Unable to find name, phone, and email address in OCR'ed document");
            }

            ci = new ContractInfo(name, phone, email);

        } catch (IOException ioe) {
            throw new ExtractException(ioe);
        }
        return ci;
    }

    /**
     * Loads the Open NLP models used for name recognition
     *
     * @throws IOException Thrown if one of the model files cannot be found
     */
    private void loadModels() throws IOException {
        InputStream modelIn = getClass().getResourceAsStream("/models/en-sent.bin");
        sentenceModel = new SentenceModel(modelIn);
        modelIn.close();

        sentenceDetector = new SentenceDetectorME(sentenceModel);

        InputStream tokenModelIn = getClass().getResourceAsStream("/models/en-token.bin");
        tokenModel = new TokenizerModel(tokenModelIn);

        tokenizer = new TokenizerME(tokenModel);
        tokenModelIn.close();

        InputStream nameModelIn = getClass().getResourceAsStream("/models/en-ner-person.bin");
        tokenNameModel = new TokenNameFinderModel(nameModelIn);

        nameFinder = new NameFinderME(tokenNameModel);
        nameModelIn.close();

    }

    /**
     * Return the sentences in the document. A sentence is based on splitting by newline
     *
     * @param document The document to process
     * @return Returns the array of sentences for the document
     * @throws IOException thrown if there is an error parsing the document
     */
    private String[] detectSentences(String document) throws IOException {

        String sentences[] = document.split("\n");

        return sentences;
    }

    /**
     * Return all tokens in the sentence
     *
     * @param sentence The sentence for token identification
     * @return Returns the tokens in the sentence
     */
    private String[] identifyTokens(String sentence) {
        return tokenizer.tokenize(sentence);

    }

    /**
     * For the tokenized "sentence", identify the names found in the sentence
     *
     * @param tokens The tokenized sentence
     * @return The names identified in the tokens
     */
    String[] identifyNames(String[] tokens) {

        Span[] names = nameFinder.find(tokens);

        ArrayList<String> retNames = new ArrayList<>();

        for (int i = 0; i < names.length; i++) {

            Span name = names[i];

            StringBuilder sb = new StringBuilder();

            for (int j = name.getStart(); j < name.getEnd(); j++) {
                if (sb.length() > 0) {
                    sb.append(' ').append(tokens[j]);
                } else {
                    sb.append(tokens[j]);
                }

            }
            retNames.add(sb.toString());
            sb.setLength(0);

        }

        return retNames.toArray(new String[retNames.size()]);
    }

    /**
     * Identify the email address, using REGEX
     * @param sentence The text to find the email address
     * @return Returns the email address found, or null if not found
     */
    String identifyEmailAddress(String sentence) {
        String emailAddress = null;

        Matcher matcher = EMAIL_REGEX.matcher(sentence);

        if (matcher.matches()) {

            emailAddress = matcher.group(1);
        }

        return emailAddress;
    }

    /**
     * Get the telephone number from the text. Optional start of Telephone: | Phone: | Tel:
     * @param token THe string to test for the phone
     * @return Returns the phone number if found, null otherwise
     */
    String identifyPhone(String token) {
        String phone = null;

        Matcher preMatch = PRE_PHONE.matcher(token);

        if (preMatch.matches()) {
            String testText = preMatch.group(3);

            testText = testText.trim();

            Matcher matcher = PHONE_REGEX.matcher(testText);

            if (matcher.matches()) {

                phone = matcher.group(1);

                phone = phone.replace(" ", "").replace("(", "");
                phone = phone.replace(")", "").replace("-", "");
                phone = phone.replace("+", "");

                if (phone.length() < 7) {
                    phone = null;
                }
            }
        }

        return phone;
    }


}
