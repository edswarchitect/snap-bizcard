package org.edbrown.snapbizcard;

import java.util.Objects;

/**
 * ContractInfo
 */
public class ContractInfo{
    /** card name */
    private String name;
    /** the telephone number */
    private String phoneNumber;
    /** the email address */
    private String emailAddress;

    /**
     * Constructor for the  business card contract
     * @param name The name of the person on the card
     * @param phoneNumber The phone number of the person on the card
     * @param emailAddress The email address of the person on the card
     */
    public ContractInfo(String name, String phoneNumber, String emailAddress) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    /*
    ContactInfo
    String getName() : returns the full name of the individual (eg. John Smith, Susan Malick)
    String getPhoneNumber() : returns the phone number formatted as a sequence of digits
    String getEmailAddress() : returns the email address

BusinessCardParser
    ContactInfo getContactInfo(String document)
     */

    /**
     * Returns the full name of the individual (eg. John Smith, Susan Malick)
     * @return Returns the full name of the individual
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the phone number formattated a sequence of digits
     * @return Returns the phone number as a sequence of digits
     */
    public String getPhoneNumber() {
        return phoneNumber;
    };

    /**
     * Returns the email address
     * @return Returns the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractInfo that = (ContractInfo) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(emailAddress, that.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phoneNumber, emailAddress);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ContractInfo{");
        sb.append("name='").append(name).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", emailAddress='").append(emailAddress).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
